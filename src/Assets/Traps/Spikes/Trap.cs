using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class Trap : MonoBehaviour
{

    public Animator spikes;
    // Start is called before the first frame update


    public GameObject triggeredEffect;

    void Start(){
        //spikes.wrapMode = WrapMode.Once;

    }
    void Update()
    {
        Keyboard kb = InputSystem.GetDevice<Keyboard>();
        if(kb.qKey.wasPressedThisFrame){
            spikes.Play("Spikes");
        }
            
    }

    
    private void OnTriggerEnter(Collider other)
    {
        
        if(other.tag == "Player"){
            Instantiate(triggeredEffect, other.transform.position, other.transform.rotation);
            spikes.Play("Spikes");
        }
    }

}
