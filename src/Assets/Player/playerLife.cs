using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerLife : MonoBehaviour
{

    public int life = 100;

    public bool isDead = false;


    // Start is called before the first frame update
    void Update()
    {
        if(life <= 0 && !isDead){
            Debug.Log("DEAD ! ");
            isDead = true;
        }
    }

    // Update is called once per frame
    public int takeDamage(int amount)
    {
        life -= amount;

        return life;
    }
}
