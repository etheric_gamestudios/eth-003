# Dungeon Crawler

## Table des matières

[[_TOC_]]

## Description

|        Type        |              Univers              |
|:------------------:|:---------------------------------:|
| Roguelite FPS coop | Fantasy “classique” ( LOTR / DnD) |

Les joueurs - chacun incarnant un héros de fantasy, doivent parcourir un donjon généré de manière aléatoire alternant salles, couloirs et arènes, contenant des monstres, des pièges, des boss et des énigmes, pour in fine attendre le maître du donjon et le vaincre. La progression des personnages à lieu entre deux expéditions, puisqu’à la mort de son héros, chaque joueur peut améliorer ses capacités afin de rendre les prochaines tentatives plus faciles et plus longues.

## Gameplay

Les phases de gameplay sont séparés en deux parties distinctes :

* Une partie **Expédition**, qui constitue la boucle principale de gameplay. C'est dans cette partie que les joueurs forment leur équipe et partent explorer le Donjon où ils devront massacrer de multitude de monstres, éviter des pièges mortels, résoudre des énigmes et affronter des boss intermédiaires, le tout dans une difficulté croissante atteignant son paroxysme lors du combat final contre le Maître du Donjon, l'entité maléfique faisant office de boss de fin. La mort en Expédition est quasiment permanente - les héros de type Support pouvant réssuciter leur alliés, mais pas plus de quelques fois par Expédition. 

* Une partie **Amélioration**, qui complémente la 1ere partie et constitue l'essence "Roguelite" du jeu. Malgré leur skill inhérent il est en effet en théorie impossible pour de nouveaux joueurs de finir le jeu en une Expédition : leurs personnages sont trop faibles, n'ont pas assez de compétences, les vagues d'ennemis sont trop nombreuses ou les combats de boss trop techniques ... une fois le groupe entier vaincu il est donc temps pour les héros de revenir à leur camp de base, et de dépenser les ressources glanées durant l'Expédition dans l'amélioration de leur équipements, compétences, et attributs.

### Joueurs

* Nombre max de joueurs ?

Chaque joueur incarne un personnage en vue FPS, disposant d’attributs tels que la vie, la valeur d’armure, la vitesse de déplacement, etc… les capacités du personnage sont définies par sa Classe, tandis que ses attributs peuvent être amenés à évoluer selon l’équipement qu’il ramasse au coup de ses expéditions

#### Système de Classe

* Respect de la Saint Trinité (**ST**) : Tank / Support / DPS
* Permettre aux joueurs de changer de classe en dehors des expéditions sans coût ou avec un coût minime => rejouabilité, nouvelle stratégie

Deux possibilités de système (pour l’instant)

##### Système classique

Système de classes comme dans la quasi-totalité des RPGs papiers ou numériques (DnD, WoW)

* (+) Ancre le joueur dans un rôle spécifique au démarrage proche des archétypes connus de tous : Mage, Guerrier… tout en proposant des originalités ou des déviations : Géomancien,Elémentaliste…
* (+) Facile à équilibrer si respect de la **ST**

* (-) “Cloisonne” les joueurs dans un rôle…
* (-) … qui peut être élargie à l’aide d’un Arbre de Talents mais qui doit
    - ne pas trop élargir le rôle pour ne pas empiéter sur les autres classes
    - être assez original si “à choix”, afin que ces derniers aient un vrai impact (et si possible éviter les builds “opti” comme dans WoW)
    - être assez large pour que le sentiment de progression soit présent
    - peut proposer des spécialisations mais cela demande encore plus de travail

Pour représenter la **ST** on peut partir d’un triangle qui donne une structure très rigide où il est facile de définir précisément le rôle de chaque classe, mais pose des difficultés pour l’insertion de potentielles nouvelles classes

![Saint Trinity Triangle](/readme_data/STTriangle.png "Saint Trinity Triangle")

Une autre possibilité est de considérer la **ST** comme un cercle, ce qui facilite l'insertion de nouvelles classes - ou les modifications des anciennes, mais rends plus compliqué la définition des rôles de chacune d'entre elles

![Saint Trinity Cercle](/readme_data/STCercle.png "Saint Trinity Cercle")

###### Exemple de classes

Basé sur un ancien système de jeu qui n'a pas vu le jour, pour un jeu sur discord en tour par tour (mais le système de classe est adaptable à un jeu en temps réel). On reste sur le schéma en triangle avec des écarts bien régulier ce qui nous donne 12 classes possibles, avec 3 classes "pures", 6 classes "typés", et 3 classes "hybrides"

* **1 - Gardien** Tank "pur" physique, annulant les CCs à son encontre et pouvant renvoyer les projectiles
* **2 - Guerrier** Tank "typé DPS" physique mono-cible, qui devient de plus en plus fort à mesure que le combat s'éternise
* **3 - Chevalier Noir** DPS "typé Tank" multi-cible, qui utilise sa vie comme ressource pour ses compétences
* **4 - Elémentaliste** DPS "pur" magique à distance, qui combine des éléments pour lancer de puissants sorts
* **5 - Voleur** DPS "typé Support", avec beaucoup d'options de mobilité lui servant à infliger des CCs et des coups critiques
* **6 - Ranger** Support "typé DPS", qui place pièges et poisons pour affaiblir les ennemis
* **7 - Prêtre** Support "pur" magique, qui boost ses alliés directement en oscillant entre les prières d'Ordre et de Chaos
* **8 - Géomancien** Support "typé Tank" magique, qui manipule le terrain pour attaquer et défendre
* **9 - Paladin** Tank "typé Support" magique, qui possède de très nombreux buffs et compétences de CC
* **10 - Moine** Tank "hybride", qui doit prévoir les attaques adverses pour les annuler et contre-attaquer
* **11 - Démoniste** DPS "hybride", qui place des DoTs et se repose sur son familier Démon
* **12 - Barde** Support "hybride", qui joue des musiques pour booster les compétences des alliés

##### Système moderne

Système “souple” à la PoE avec un Arbre de Talents dans lequel le joueur peut dépenser des points pour débloquer compétences actives et passives

* (+) Plus d’originalité dans les builds, si l’Arbre est bien fait les joueurs peuvent être amenés à créer des dizaines de build différents
* (+) Facile d'ajouter de nouvelles compétences “à la volée” sans avoir à l’associer forcément à une classe spécifique
 
* (-) Très dur à équilibrer pour à la fois respecter la ST, et éviter les builds “opti” (comme sur PoE) puisque la liberté est totale mais le nombre de “points” à dépenser est lui limité, amenant à la création de beaucoup de builds non viables ou trop faibles
* (-) Pas de sentiment d’appartenance fort à un archétype chez les joueurs

### Donjon

Le Donjon est le nom donné à la zone de jeu dans laquelle les joueurs partent en Expédition, et vont donc passer le plus clair de leur temps de jeu

#### Progression

Durant leur Expéditions, les joueurs vont parcourir ce qui est basiquement une succession de salles, remplie d'évènement pseudo-aléatoire dont la difficulté va crescendo, de manière non linéaire mais avec un essor maintenu et de potentiel plateaux, dans le but d'un côté de ne pas rendre la progression des joueurs trop monotone et prévisible, et de l'autre d'éviter des évènements trop brutaux qui pourraient être pris comme des injustices du système de jeu et donc générer de la frustrations

On peut donc partir sur une courbe de difficulté polynômiale comme par exemple

```py
x = y^2
```

#### Type d'Evènement

On peut diviser les évènements de manière générique en quelques catégories :

* Combat contre des monstres
* Résolution d'un énigme
* Combat contre un boss
* Passage dans des pièges
* Découverte de trésors
* Appréciation du paysage / Pause et repos

Avec une granularité plus fine sur ces catégories afin de diversifier l'expérience de jeu : par exemple la catégorie "Combat contre des monstres" peut contenir tout les événements suivants :

- Attaque d'un camp de monstres fortifié
- Embuscade surprise par une salle dérobée
- Combat chronométré avant invocation d'une Horde de renforts ...

De plus il est possible de "chaîner" les évènements en faisant que la complétion positive ou négative d'un 1er évènement influence sur un 2nd évènement, par exemple :

- Résoudre une énigme en moins de x secondes trigger un second évènement qui ouvre une salle secrète, qui serait resté fermée dans le cas contraire
- Echouer à sauver un otage des monstres entraine l'invocation d'un mini-boss dans la salle, ce qui ne serait pas arrivé dans le cas contraire ...

#### Génération

Pour une expérience renouvelable le Donjon doit être au maximum généré de manière procédurale. A priori la meilleure des façons de faire serait de créer des prefabs ou "blocs" de Donjon correspondant à des salles, et à les assembler les uns avec les autres en pensant bien à espacer les prefabs similaires (par exemple éviter que 2 prefabs de mini-boss se trouvent l'un après l'autre) pour offrir une expérience qui semble à la fois différente d'une Expédition à une autre, mais également d'une salle à une autre

Il faut par contre définir si les prefabs ne sont que des décors que le génératuer remplie *après 1ere génération* avec des évènements, ou si les prefabs sont de vrais "blocs" complet contenant la salle et l'évènement associé. Le 1er cas est plus souple et meilleur pour la rejouabilité mais nécessite un algorithme de génération un peu plus fin

De même il faut définir la dimension et "l'aspect" des salles, puisque plusieurs paramètres peuvent rentrer en jeu dans ce processus :

* Présence de classes à distance ? Si oui alors il faut éviter, sauf en cas d'évènement particulier, de faire des salles trop petites et / ou trop encombrées qui pourraient abimer l'expérience de jeu de ces joueurs
* Verticalité du Donjon ? Selon le degré de verticalité il faut alors prévoir de remplir ces salles avec des éléments permettant leur ascension, et aussi prendre en compte de manière plus fine la 3eme dimension verticale lors de la génération procédurale - pour éviter par exemple d'avoir une salle clippée dans une autre
* Cloisonnement du Donjon ? Peut-on proposer des salles "ouvertes" sur l'extérieur qui font office de bouffer d'air pour les joueurs, mais à quel prix sur les performances et la cohésion

Pour faciliter cette génération on peut partir d'une structure en graphe représentant l'ensemble des salles et leur liaisons, que l'on complète pas un graphe secondaire d'évènement - ce qui sera d'autant plus utile pour suivre le chaînage de ces derniers.

**Map Graph**

```mermaid
graph TD
    A["Start<br>lvl: 0<br>events:()"] --> B("Cave entrance<br>lvl:0<br>events:(0)")
    B --> C("Cave<br>lvl:1<br>events:(1,2)")
    C --> D("Mine corridor<br>lvl:2<br>events:(3,5)")
    C -->|"events=6"| E("Crystal Cave<br>lvl:4<br>events:(17)")
    D --> F("Mine tracks<br>lvl:4<br>events:(4)")
```

**Events Graph**

```mermaid
graph TD
    A["ID: 0<br>Type: Monster<br>Sub-type: Ambush"]
    B["ID: 1<br>Type: Monster<br>Sub-type: Sacrifice"] --> |"Time>=120"| C["ID:2<br>Type: Boss<br>Sub-type:Sacrifice Invocation"]
    B --> |"Time<120"| D["ID=6<br>Type: Treasure<br>Sub-type:Secret Room"]
    D --> E["ID=17<br>Type: Treasure<br>Sub-type:Equipement"]
    F["ID: 3<br>Type: Trap<br>Sub-type:Hide & Run"] --> |"death>0"| G["ID:5<br>Type: Monster<br>Sub-type:Ambush"]
    H["ID:4<br>Type: Puzzle<br>Sub-type:Button sequence"]
```

## Idées Game Design

En fin d'expédition, le joueur repars au Hub avec des points d'XP et une partie (si mort) ou totalité (si abandon du run) des composants, matériaux et équipement looté durant le run

Activité Hub : En plus de progression perso (point d'xp / arbre de talent), possibilité d'utiliser ces composants / matériaux pour crafter de l'équipement de meilleure qualité, offrant des capacités supplémentaires

Possibilité de "recycler" les équipements temporaires (basiquement armes et armures looté hors coffre et trésor) récupérés durant le run en scrap, pour crafter des composants ou matériaux que le joueur n'a pas eu la chance d'avoir pendant le run (afin de limiter un peu l'aléatoire)

